resource "local_file" "params" {
  for_each = local.functions

  content = jsonencode(each.value.params)
  filename = "${each.value.source}/params.json"
}