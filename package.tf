data "archive_file" "lambda_zip" {
  for_each = local.functions

  source_dir = each.value.source

  # Dirty trick to enforce correct dependency direction
  # since depends_on doesn't support many-many
  type             = "zip${substr(local_file.params[each.key].filename, 0, 0)}"
  output_file_mode = "0666"
  output_path      = "${path.module}/archives/${each.key}.zip"
}
